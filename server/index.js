const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const app = express();
const server = http.Server(app);
const io = socketIO(server);
const port = process.env.PORT || 3000;

io.on('connection', (socket) => {
    console.log('user connected');

    socket.on('message', (data) => {
        io.emit('message', data);
    });
});

server.listen(port, () => {
    console.log(`Started on port: ${port}`);
})

