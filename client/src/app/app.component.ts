import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from './services/chat.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'DailyChatUp';
  user: string;
  message: string;
  messages: string[] = [];
  private chatSub: Subscription;

  constructor(private chatService: ChatService) {}

  public sendMessage(): void {
    this.chatService.sendMessage(this.user, this.message);
    this.message = '';
  }

  ngOnInit(): void {
    this.chatSub = this.chatService.getMessages().subscribe( (data: {user: string, message: string}) => {
      this.messages.push(`${data.user} írta: ${data.message}`);
    });
  }
  ngOnDestroy(): void {
    this.chatSub.unsubscribe();
  }

}
