import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const BACKEND_URL = environment.apiUrl;

@Injectable({providedIn: 'root'})
export class ChatService {
  private socket;

  constructor() {
    this.socket = io(BACKEND_URL);
  }

  public sendMessage(user: string, message: string) {
    const data = {
      user,
      message
    };
    this.socket.emit('message', data);
  }

  public getMessages() {
    return new Observable((observer) => {
      this.socket.on('message', (data) => {
        observer.next(data);
      });
    });
  }
}
